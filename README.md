This is a very early stage of Gitlab HA Omnibus with encryption in transit NFS server

Assumptions:
You will change s3 path to your path
The role for the servers will have access to this s3 bucket

NFS Server Validation:
Files created during userdata to check:
/etc/fstab
/etc/exports
/etc/stunnel/nfs-tls.pem
/etc/systemd/system/MC-nfsd.socket
/etc/systemd/system/MC-nfsd@.service
/etc/stunnel/MC-nfsd.conf
/tmp/orig.config
/tmp/3d-nfsd.conf  (copy of /tmp/orig.config replace with nfs server IP address to connect)
Files sent to S3 to check (insure paths match your environment):

Application Server Validation:
files created during userdata to check:
/etc/systemd/system/3d-nfsd.socket
/etc/systemd/system/3d-nfsd@.service
/etc/fstab
Files pulled from S3 to check (insure s3 path match your environment)
aws s3 cp s3://iqt-gitlab/config/nfs-tls.pem /etc/stunnel/nfs-tls.pem
aws s3 cp s3://iqt-gitlab/config/3d-nfsd /etc/stunnel/3d-nfsd.conf
cp s3://gitlab-bucket/gitlab-prod-rb.txt /etc/gitlab/gitlab.rb

Order of cloudformation operations:
NFS,  postgress, redis
Prometheus
Then: Configure gitlab.rb for app server push to s3
run appserver 

In the gitlab.rb
Set path for s3
remove email or change email configuration to your environment
configure redis host to the dns of redis cluster after running the redis cf template
configure postgres to the dns of the RDS cluster after running the rds cf template
Nginx needs to be modified with certs.  I don't have enough information on your certs to manage this

gitlab-omnibus-rev4.json is the grafana dashboard 

