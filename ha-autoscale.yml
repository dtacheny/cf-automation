AWSTemplateFormatVersion: '2010-09-09'
Description: "Deploy Gitlab application Instance in an autoscale group"
Parameters:
  pKeyName:
    Type: String
    Description: "Key pair to be used for the EC2 instance"
  pAppID:
    Type: String
    Default: "gitlab-app"
    Description: "Tag name for the EC2 instance"
  pIamRole:
    Type: String
    Default: "gitlab-app-role"
    Description: "IAM role to be applied to the EC2 instance"
  pImageID:
    Type: AWS::EC2::Image::Id
    Default: "ami-013be31976ca2c322"
    Description: "AMI ID to be used as the base image for the EC2 instance"
  pVpcId:
    Type: AWS::EC2::VPC::Id
    Description: "VPC ID of your existing Virtual Private Cloud (VPC)"
  pCIDR:
    Type: String
    Default: 0.0.0.0/0
    Description: "CIDR block to restrict security group access to the application instance."
    AllowedPattern: ^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|[1-2][0-9]|3[0-2]))$
    MinLength: 9
    MaxLength: 18
    ConstraintDescription: 'CIDR block is malformed.'    
  pSubnetIDA:
    Type: AWS::EC2::Subnet::Id
    Description: The first subnet for your autoscaling group in your Virtual Private Cloud (VPC)
    ConstraintDescription: >-
      must be a list of at least two existing subnets associated with at least
      two different availability zones. They should be residing in the selected
      Virtual Private Cloud.
  pSubnetIDB:
    Type: AWS::EC2::Subnet::Id
    Description: The second subnet for your autoscaling group in your Virtual Private Cloud (VPC)
    ConstraintDescription: >-
      must be a list of at least two existing subnets associated with at least
      two different availability zones. They should be residing in the selected
      Virtual Private Cloud.
  pInstanceType:
    Type: String
    Default: t2.micro
    Description: "The application EC2 instance type."
    AllowedValues: 
      - t2.micro
      - t2.small
      - t2.large
      - m4.large
      - m4.xlarge
      - m4.2xlarge
Conditions:
  UseKeypair:
    Fn::Not:
    - Fn::Equals:
      - ""
      - Ref: pKeyName
Resources:
  rSecurityGroupELB:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Enable HTTP/HTTPS access on port 443/443 80/80 22/22
      VpcId:
        Ref: pVpcId
      SecurityGroupIngress:
      - IpProtocol: tcp
        FromPort: 80
        ToPort: 80
        CidrIp: 0.0.0.0/0
      - IpProtocol: tcp
        FromPort: 443
        ToPort: 443
        CidrIp: 0.0.0.0/0
      - IpProtocol: tcp
        FromPort: 22
        ToPort: 22
        CidrIp: 0.0.0.0/0
  rSecurityGroupApp:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: "Security group for AutoScale"
      VpcId:
        Ref: pVpcId
      SecurityGroupIngress:
      - IpProtocol: tcp
        CidrIp:
          Ref: pCIDR
        FromPort: 80
        ToPort:  80
      - IpProtocol: tcp
        CidrIp:
          Ref: pCIDR
        FromPort: 443
        ToPort: 443
      - IpProtocol: tcp
        CidrIp:
          Ref: pCIDR
        FromPort: 6379
        ToPort: 6379
      - IpProtocol: tcp
        CidrIp:
          Ref: pCIDR
        FromPort: 22
        ToPort: 22
  rLoadBalancer:
    Type: AWS::ElasticLoadBalancing::LoadBalancer
    Properties:
      LoadBalancerName:
        Fn::Join:
        - "-"
        - - Ref: pAppID
          - Ref: pVpcId
      SecurityGroups:
      -  Ref: rSecurityGroupELB
      Subnets: [ Ref: pSubnetIDA, Ref: pSubnetIDB ]
      CrossZone: true
      Listeners:
      - LoadBalancerPort: '80'
        InstancePort: '80'
        Protocol: HTTP
      - LoadBalancerPort: '22'
        InstancePort: '22'
        Protocol: TCP
      HealthCheck:
        Target:
          Fn::Join:
          - ''
          - - HTTP:80
            - "/explore"
        HealthyThreshold: '5'
        UnhealthyThreshold: '3'
        Interval: '30'
        Timeout: '5'
  rLaunchConfiguration:
    Type: AWS::AutoScaling::LaunchConfiguration
    Properties:
      UserData:
        Fn::Base64: 
            Fn::Join:
                - " "
                - - |
                    #!/bin/bash -ex
                  - |
                    sudo yum update -y
                  - |
                    sudo yum install -y curl policycoreutils-python postfix awscli stunnel nfs-utils 
                  - |
                    sudo systemctl enable postfix
                  - |
                    sudo systemctl start postfix
                  - |
                    sudo mkdir -p /var/opt/nfs/mnt/
                  - |
                    sudo mkdir -p /var/empty/stunnel
                  - |
                    sudo chmod 777 /var/opt/nfs/mnt/
                  - |
                    sudo aws s3 cp s3://iqt-gitlab/config/nfs-tls.pem /etc/stunnel/nfs-tls.pem
                  - |
                    sudo aws s3 cp s3://iqt-gitlab/config/3d-nfsd /etc/stunnel/3d-nfsd.conf
                  - |
                    sudo chmod 400 /etc/stunnel/nfs-tls.pem 
                  - |
                    sudo printf '%s\n' '[Unit]' 'Description=NFS over stunnel/TLS server' ' ' '[Socket]' 'ListenStream=2323' 'Accept=yes' ' ' '[Install]' 'WantedBy=sockets.target' > /etc/systemd/system/3d-nfsd.socket
                  - |
                    sudo printf '%s\n' '[Unit]' 'Description=NFS over stunnel/TLS server' ' ' '[Service]' 'ExecStart=-/bin/stunnel /etc/stunnel/3d-nfsd.conf' 'StandardInput=socket'  > /etc/systemd/system/3d-nfsd@.service
                  - |
                    sudo systemctl start 3d-nfsd.socket
                  - |
                    sudo systemctl enable 3d-nfsd.socket
                  - |
                    sudo echo "127.0.0.1:/var/opt/nfs       /var/opt/nfs      nfs4 defaults,soft,rsize=1048576,wsize=1048576,noatime,nofail,lookupcache=positive 0 2" >> /etc/fstab
                  - |
                    sudo mount -a
                  - |
                    sudo curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
                  - |
                    sudo EXTERNAL_URL="http://gitlab-opp.tanuki.cloud" yum install -y gitlab-ee
                  - |
                    sudo aws s3 cp s3://gitlab-bucket/gitlab-prod-rb.txt /etc/gitlab/gitlab.rb
                  - |
                    sudo gitlab-ctl stop
                  - |
                    sudo gitlab-rake cache:clear
                  - |
                    sudo gitlab-ctl reconfigure
                  - |
                    sudo echo yes|sudo gitlab-rake gitlab:shell:setup
                  - |
                    sudo gitlab-ctl restart
      InstanceType:
        Ref: pInstanceType
      ImageId:
        Ref: pImageID
      KeyName:
        Fn::If:
        - UseKeypair
        - Ref: pKeyName
        - Ref: AWS::NoValue
      IamInstanceProfile:
        Ref: pIamRole
      AssociatePublicIpAddress: false
      SecurityGroups:
      -  Ref: rSecurityGroupApp
      BlockDeviceMappings:
      - DeviceName: "/dev/xvda"
        Ebs:
          VolumeSize: 20
    DependsOn: rLoadBalancer
  rAutoScalingGroup:
    Type: AWS::AutoScaling::AutoScalingGroup
    Properties:
      VPCZoneIdentifier: [ Ref: pSubnetIDA, Ref: pSubnetIDB ]
      LaunchConfigurationName:
        Ref: rLaunchConfiguration
      LoadBalancerNames:
      -  Ref: rLoadBalancer
      MinSize: '4'
      MaxSize: '20'
      Tags: 
        - 
          Key: "Name"
          Value: "DevX Gitlab Application ASG"
          PropagateAtLaunch: true 
        - 
          Key: "Environment"
          Value: "Production"
          PropagateAtLaunch: true
  rScalingPolicyUp:
    Type: AWS::AutoScaling::ScalingPolicy
    Properties:
      AdjustmentType: ChangeInCapacity
      AutoScalingGroupName: 
        Ref: rAutoScalingGroup
      Cooldown: '60'
      ScalingAdjustment: 1
  rScalingPolicyDown:
    Type: AWS::AutoScaling::ScalingPolicy
    Properties:
      AdjustmentType: ChangeInCapacity
      AutoScalingGroupName: 
        Ref: rAutoScalingGroup
      Cooldown: '60'
      ScalingAdjustment: -1
  rCPUAlarmHigh:
    Type: AWS::CloudWatch::Alarm
    Properties:
      AlarmName: !Sub '${AWS::StackName}-gitlab-CPUHigh'
      AlarmDescription: Scale-up if CPU > 90% for 15 minutes
      MetricName: CPUUtilization
      Namespace: AWS/EC2
      Statistic: Average
      Period: 300
      EvaluationPeriods: 3
      Threshold: 90
      AlarmActions:
      -  Ref: rScalingPolicyUp
      Dimensions:
        -  Name: AutoScalingGroupName
           Value: !Ref rAutoScalingGroup
      ComparisonOperator: GreaterThanThreshold
  rCPUAlarmLow:
    Type: AWS::CloudWatch::Alarm
    Properties:
      AlarmName: !Sub '${AWS::StackName}-gitlab-CPULow'
      AlarmDescription: Scale-down if CPU < 70% for 15 minutes
      MetricName: CPUUtilization
      Namespace: AWS/EC2
      Statistic: Average
      Period: 300
      EvaluationPeriods: 3
      Threshold: 70
      AlarmActions:
      -  Ref: rScalingPolicyDown
      Dimensions:
        -  Name: AutoScalingGroupName
           Value: !Ref rAutoScalingGroup
      ComparisonOperator: LessThanThreshold
Outputs:
  oInstanceId:
    Description: "Instance Id of the newly created application EC2 instance"
    Value:
      Ref: rAutoScalingGroup
