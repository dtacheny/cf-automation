AWSTemplateFormatVersion: '2010-09-09'
Description: "Deploy Gitlab application Instance"
Parameters:
  pKeyName:
    Type: String
    Description: "Key pair to be used for the EC2 instance"
  pAppID:
    Type: String
    Default: "gitlab-app"
    Description: "Tag name for the EC2 instance"
  pAssignPublicIP:
    Type: String
    Default: "true"
    Description: "Assign a public IP address to the instance?"
    AllowedValues:
      - 'true'
      - 'false'
  pIamRole:
    Type: String
    Default: "gitlab-app-role"
    Description: "IAM role to be applied to the EC2 instance"
  pInstanceName:
    Type: String
    Default: "GitLab: App"
    Description: "Name for the EC2 instance"
  pImageID:
    Type: AWS::EC2::Image::Id
    Default: "ami-013be31976ca2c322"
    Description: "AMI ID to be used as the base image for the EC2 instance"
  pVpcId:
    Type: AWS::EC2::VPC::Id
    Description: "VPC ID of your existing Virtual Private Cloud (VPC)"
  pCIDR:
    Type: String
    Default: 0.0.0.0/0
    Description: "CIDR block to restrict security group access to the NFS instance."
    AllowedPattern: ^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|[1-2][0-9]|3[0-2]))$
    MinLength: 9
    MaxLength: 18
    ConstraintDescription: 'CIDR block is malformed.'    
  pSubnetID:
    Type: AWS::EC2::Subnet::Id
    Description: "The subnet where the application EC2 instance will reside"
  pInstanceType:
    Type: String
    Default: t2.micro
    Description: "The application EC2 instance type."
    AllowedValues: 
      - t2.micro
      - t2.small
      - t2.large
      - m4.large
      - m4.xlarge
      - m4.2xlarge
  pEc2Env:
    Type: String
    Default: test
    Description: "Environment tag"
    AllowedValues:
     - dev
     - test
     - stage
     - prod
  pAvailabilityZone:
    Type: AWS::EC2::AvailabilityZone::Name
    Default: "us-east-1a"
    Description: "Availability zone to deploy application EC2 instance"
  pApplicationSecurityGroupName:
    Type: String
    Default: "DevX Application Security Group"
Conditions:
  UseKeypair:
    Fn::Not:
    - Fn::Equals:
      - ""
      - Ref: pKeyName
Resources:
  rAppOpsSG:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupName: 
        Ref: pApplicationSecurityGroupName
      GroupDescription: "Security group for application instance"
      VpcId:
        Ref: pVpcId
      SecurityGroupIngress:
      - IpProtocol: tcp
        CidrIp:
          Ref: pCIDR
        FromPort: '80'
        ToPort:  '80'
      - IpProtocol: tcp
        CidrIp:
          Ref: pCIDR
        FromPort: '443'
        ToPort: '443'
      - IpProtocol: tcp
        CidrIp:
          Ref: pCIDR
        FromPort: '22'
        ToPort: '22'
      Tags:
        - 
          Key: "Name"
          Value: 
            Ref: pApplicationSecurityGroupName   
  rEC2Instance:
    Type: AWS::EC2::Instance
    Properties:
      UserData:
        Fn::Base64: 
            Fn::Join:
                - " "
                - - |
                    #!/bin/bash -ex
                  - |
                    sudo yum update -y
                  - |
                    sudo yum install -y curl policycoreutils-python postfix awscli
                  - |
                    sudo systemctl enable postfix
                  - |
                    sudo systemctl start postfix
                  - |
                    sudo curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
                  - |
                    sudo EXTERNAL_URL="http://gitlab-opp.tanuki.cloud" yum install -y gitlab-ee
                  - |
                    sudo aws s3 cp s3://$BUCKET_NAME/gitlab-ops.rb /etc/gitlab/gitlab.rb
                  - |
                    sudo gitlab-ctl stop
                  - |
                    sudo gitlab-rake cache:clear
                  - |
                    sudo gitlab-ctl reconfigure
                  - |
                    sudo sudo echo yes|sudo gitlab-rake gitlab:shell:setup
                  - |
                    sudo gitlab-ctl restart
      InstanceType:
        Ref: pInstanceType
      AvailabilityZone:
        Ref: pAvailabilityZone
      ImageId:
        Ref: pImageID
      IamInstanceProfile:
        Ref: pIamRole
      KeyName:
        Fn::If:
        - UseKeypair
        - Ref: pKeyName
        - Ref: AWS::NoValue
      NetworkInterfaces:
      - AssociatePublicIpAddress: 
          Ref: pAssignPublicIP
        DeviceIndex: '0'
        SubnetId:
          Ref: pSubnetID
        GroupSet:
        -  Ref: rAppOpsSG
      BlockDeviceMappings:
      - DeviceName: "/dev/xvda"
        Ebs:
          DeleteOnTermination: false
          VolumeSize: '10'
      Tags:
      - Key: Vsad
        Value:
          Ref: pAppID
      - Key: Name
        Value:
          Ref: pInstanceName
      - Key: OS
        Value: Linux
      - Key: Environment
        Value:
          Ref: pEc2Env
      - Key: Nostop
        Value: 'true'
Outputs:
  oInstanceId:
    Description: "Instance Id of the newly created application EC2 instance"
    Value:
      Ref: rEC2Instance
  oAZ:
    Description: "Availability Zone of the newly created application EC2 instance"
    Value:
      Fn::GetAtt:
      - rEC2Instance
      - AvailabilityZone
  oPrivateIP:
    Description: "Private IP of the newly created application EC2 instance"
    Value:
      Fn::GetAtt:
      - rEC2Instance
      - PrivateIp
