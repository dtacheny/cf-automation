#!/bin/bash

./uploadTemplates.sh

aws cloudformation create-stack --stack-name "devx-app" \
--template-url "$S3_HTTP_URL/$BUCKET_NAME/devx-app.yml" \
--parameters ParameterKey=pVpcId,ParameterValue="$VPC" \
ParameterKey=pSubnetID,ParameterValue="$SUBNET_ID" \
ParameterKey=pKeyName,ParameterValue="$KEY_NAME" \
ParameterKey=pAvailabilityZone,ParameterValue="$AVAILABILITY_ZONE"
