#!/bin/bash

aws s3 cp cf-templates/gitlab-app/devx-elasticache.yml s3://$BUCKET_NAME -sse

aws cloudformation create-stack --stack-name "devx-elasticache" \
--template-url "$S3_HTTP_URL/$BUCKET_NAME/devx-elasticache.yml" \
--parameters ParameterKey=pVpcId,ParameterValue="$VPC" \
ParameterKey=pRedisSubnet,ParameterValue="$REDIS_SUBNET" \
ParameterKey=pJobIdentifier,ParameterValue="$JOB_IDENTIFIER" \
ParameterKey=pNumNodeGroups,ParameterValue="$NUM_NODE_GROUPS" \
ParameterKey=pReplicasPerNodeGroup,ParameterValue="$REPLICAS_PER_NODE_GROUP" 
