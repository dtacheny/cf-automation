#!/bin/bash

aws s3 cp cf-templates/gitlab-app/devx-nfs.yml s3://$BUCKET_NAME --sse

aws cloudformation create-stack --stack-name "devx-nfs" \
--template-url "$S3_HTTP_URL/$BUCKET_NAME/devx-nfs.yml" \
--parameters ParameterKey=pVpcId,ParameterValue="$VPC" \
ParameterKey=pSubnetID,ParameterValue="$SUBNET_ID" \
ParameterKey=pKeyName,ParameterValue="$KEY_NAME" \
ParameterKey=pAvailabilityZone,ParameterValue="$AVAILABILITY_ZONE"
