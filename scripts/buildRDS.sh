#!/bin/bash

aws s3 cp cf-templates/gitlab-app/devx-rds-postgres.yml s3://$BUCKET_NAME --sse

aws cloudformation create-stack --stack-name "devx-rds" \
--template-url "$S3_HTTP_URL/$BUCKET_NAME/devx-rds-postgres.yml" \
--parameters ParameterKey=pVpcId,ParameterValue="$VPC" \
ParameterKey=pSubnetA,ParameterValue="$SUBNET_A" \
ParameterKey=pSubnetB,ParameterValue="$SUBNET_B" \
ParameterKey=pDBPassword,ParameterValue="$DB_PASSWORD"
