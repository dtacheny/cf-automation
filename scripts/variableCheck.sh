#!/bin/sh
fail=false
while read var; do
  val=$(printenv $var)
  if [ -z "${val}" ]; then
    echo "$var is not set"
    fail=true
  fi
done <../../VARIABLES

if [ "$fail" == true ]; then
    exit 1
fi

echo "All variables are present"